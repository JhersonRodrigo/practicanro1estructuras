/**
 * Realizar un programa que te realice la suma, la resta, la división y la multiplicación de dos números pedidos por pantalla y el resultado debe ser mostrado por pantalla
 * @author Jherson Rodrigo Mamani Poma
 */
package com.mycompany.practicanro1;
import javax.swing.JOptionPane;

public class ejercicio1 {
    
    public static void main(String[] args){
        String n1,n2,resultado;
        int N1,N2,suma,resta,multiplicacion,division;
        n1 = JOptionPane.showInputDialog("Introduce el primer numero");
        N1 = Integer.parseInt(n1);
        n2 = JOptionPane.showInputDialog("Introduce el segundo numero");
        N2 = Integer.parseInt(n2);
        
        suma=N1+N2;
        resta=N1-N2;
        multiplicacion=N1*N2;
        division=N1/N2;
        
        resultado=("La Suma es: "+suma+ "\nLa Resta es: "+resta+"\nLa Multiplicacion es: "+multiplicacion+"\nLa Division es: "+division);
        JOptionPane.showMessageDialog(null, resultado);
        
     
       
    }
}      