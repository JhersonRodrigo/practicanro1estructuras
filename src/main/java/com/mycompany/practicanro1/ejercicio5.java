/**
 *Realizar un programa que lea dos números por teclado y muestre el resultado de la división del primer número por el segundo. 
 * Se debe comprobar que el divisor no puede ser cero.
 * @author Jherson Rodrigo Mamani Poma
 */
package com.mycompany.practicanro1;
import javax.swing.JOptionPane;

public class ejercicio5 {

    public static void main(String[] args) {
        
        
        double dividendo, divisor;
        String x = JOptionPane.showInputDialog("Introduzca el dividendo: ");
        dividendo = Double.parseDouble(x);
        String y = JOptionPane.showInputDialog("Introduzca el divisor: ");
        divisor = Double.parseDouble(y);
        if(divisor==0){
           JOptionPane.showMessageDialog(null,"No se puede dividir entre cero");   
        }else{
            JOptionPane.showMessageDialog(null,"El resultado de la division de: "+dividendo + " / " + divisor + " \nEs igual a = " + dividendo/divisor);  
             
        }
    }
}
