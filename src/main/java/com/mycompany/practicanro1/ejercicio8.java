/**
 * Realizar un programa que nos convierta una cantidad de Bolivianos introducida por teclado a otra moneda, 
 * estas pueden ser a dolares, euros o libras. El método tendrá como parámetros, la cantidad de bolivianos y 
 * la moneda a pasar que sera una cadena, este no devolverá ningún valor, mostrara un mensaje indicando el cambio.
 * @author Jherson Rodrigo Mamani Poma
 */

package com.mycompany.practicanro1;
import javax.swing.JOptionPane;
import java.text.DecimalFormat;

 
public class ejercicio8 {
    public static void main(String[] args) {
        String t=JOptionPane.showInputDialog("Escribe la cantidad en Bolivianos");
        double c=Double.parseDouble(t);
        String m=JOptionPane.showInputDialog("Escribe la moneda a la que quieres convertir");
        JOptionPane.showMessageDialog(null, convert(c, m));
    }
    public static String convert (double c, String m){
        double res=0;
 
   
        switch (m){
        case "libras":
            res=c*0.12;
            break;
        case "dolares":
            res=c*0.14;
            break;
        case "euros":
            res=c*0.13;
            break;
        default:
           JOptionPane.showMessageDialog(null, "error");
        }
        DecimalFormat df = new DecimalFormat("#.00");
        return c+ " bolivianos en " +m+ " son " +df.format(res);
        
 
    }
}
