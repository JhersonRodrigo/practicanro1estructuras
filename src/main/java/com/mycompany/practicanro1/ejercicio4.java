/**
 * Realizar unprograma que lea un carácter por teclado y compruebe si es una letra mayúscula
 * @author Jherson Rodrigo Mamani Poma
 */
package com.mycompany.practicanro1;
import java.io.IOException;

public class ejercicio4 {

    public static void main(String[] args) throws IOException {
        char car;
        System.out.print("Introduzca un carácter: ");
        car = (char) System.in.read(); 

        if (Character.isUpperCase(car))    
        {
            System.out.println("Es una letra mayúscula");
        } else {
            System.out.println("No es una letra mayúscula");
        }
    }
}