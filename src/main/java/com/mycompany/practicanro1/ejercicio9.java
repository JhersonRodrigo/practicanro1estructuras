/**
 * Realizar un programa Java que calcule el área de un triángulo en función de las longitudes de sus lados (a, b, c), 
 * según la siguiente fórmula: Area =RaizCuadrada(p*(p-a)*(p-b)*(p-c)) donde p = (a+b+c)/2 Para calcular la raíz cuadrada se utiliza el método Math.sqrt()
 * @author Jherson Rodrigo Mamani Poma
 */
package com.mycompany.practicanro1;
import javax.swing.JOptionPane;

public class ejercicio9 {

    public static void main(String[] args) {
        double p;

        String A = JOptionPane.showInputDialog("Introduzca longitud del primer lado del triángulo: ");
        double a = Double.parseDouble(A);

        String B = JOptionPane.showInputDialog("Introduzca longitud del segundo lado del triángulo: ");
        double b = Double.parseDouble(B);

        String C = JOptionPane.showInputDialog("Introduzca longitud del tercer lado del triángulo: ");
        double c = Double.parseDouble(C);

        p = (a+b+c)/2;
        
        JOptionPane.showMessageDialog(null," El Area es igual a = " +  Math.sqrt(p*(p-a)*(p-b)*(p-c)));
    }
}
