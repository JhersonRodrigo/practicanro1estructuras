/**
 * Realizar un programa que calcule el sueldo de un trabajador, el programa va a solicitar el numero de horas que ha trabajado en un mes, 
 * las horas se pagan a 10Bs.
 * @author Jherson Rodrigo Mamani Poma
 */
package com.mycompany.practicanro1;
import javax.swing.JOptionPane;
 
public class ejercicio3 {
 
    public static void main(String[] args) {
    int sueldo=0;
   
    String inputtext=JOptionPane.showInputDialog("Ingrese las horas trabajadas en el mes");
    int horas = Integer.parseInt(inputtext);
  
    sueldo = horas*10;
    JOptionPane.showMessageDialog(null,"El sueldo es de: "+sueldo);
 }
}
  
  