/**
 * Realizar un programa que nos convierta un número en base decimal a binario. Esto lo realizara un método al que le pasaremos 
 * el numero como parámetro, devolverá un String con el numero convertido a binario. Para convertir un numero decimal a binario, 
 * debemos dividir entre 2 el numero y el resultado de esa división se divide entre 2 de nuevo hasta que no se pueda dividir mas, 
 * el resto que obtengamos de cada división formara el numero binario, de abajo a arriba.
 * @author Jherson Rodrigo Mamani Poma
 */
package com.mycompany.practicanro1;
import javax.swing.JOptionPane;
public class ejercicio6 {
    public static void main(String[]args){
        String inputtext = JOptionPane.showInputDialog("Introduce un numero");
        int number = Integer.parseInt(inputtext);
        String result = decimaltoBinary (number);
        JOptionPane.showMessageDialog(null, "El numero: "+number+" en binario es: "+result);
    }
    
    public static String decimaltoBinary(int number){
        String binary = "";
        String dig;
        //8/2 = 4 4/2 = 2
        // 0       0
        for(int i=number; i>0; i/=2){
            if(i%2==1){
                dig = "1";
            }else{
                dig = "0";
            }
            binary=dig+binary;
        }
        return binary;
    }
  
}
