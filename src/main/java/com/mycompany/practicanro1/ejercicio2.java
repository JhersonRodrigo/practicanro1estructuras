/**
 * Realizar un programa que te realice el promedio de las notas de un alumno, para ello el programa te va a tener que solicitar el nombre del alumno y 
 * las notas de las 3 evaluaciones.
 * @author Jherson Rodrigo Mamani Poma
 */
package com.mycompany.practicanro1;
import java.util.Scanner;
 
public class ejercicio2 {
 
 public static void main(String[] args) {
   
  Scanner entrada = new Scanner (System.in);
   
  String nombre_alumno;
  double evaluacion1;
  double evaluacion2;
  double evaluacion3;
  double evaluacion_final;  
   
  System.out.println("Nombre del alumno: ");
  nombre_alumno = entrada.nextLine();
   
  System.out.println("Primera Nota de evaluación: ");
  evaluacion1 = entrada.nextDouble();
   
  System.out.println("Segunda Nota de evaluación: ");
  evaluacion2 = entrada.nextDouble();
   
  System.out.println("Tercera Nota de evaluación: ");
  evaluacion3 = entrada.nextDouble();
   
  evaluacion_final = (evaluacion1 + evaluacion2 + evaluacion3)/3;  
   
  System.out.println("La nota media de " + nombre_alumno + " es " + evaluacion_final);
 }
}
