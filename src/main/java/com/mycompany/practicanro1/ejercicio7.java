/**
 * Realizar un programa que cuente el número de cifras de un número entero positivo (hay que controlarlo) pedido por teclado. Crea un método que realice esta acción,
 * @author Jherson Rodrigo Mamani Poma
 */
package com.mycompany.practicanro1;
import javax.swing.JOptionPane;
public class ejercicio7 {
    public static void main(String[]args){
        
        int number = 0;
        String inputtext = JOptionPane.showInputDialog("Introduce un numero");
        number = countnumber(Integer.parseInt(inputtext));
        JOptionPane.showMessageDialog(null, "el numero de cifras que tiene el numero: "+inputtext+" es: "+number);
    } 
    
    public static int countnumber(int number){
        //100/10=10
        //10/10=1
        //1/10=0
        int count = 0;
        for(int i=number; i>0;i/=10){
            count++;
        }
        return count;
    }
}
   